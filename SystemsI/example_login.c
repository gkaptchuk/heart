#include "stdio.h"
#include "string.h"

int password_cmp(char* password, char* attempt)
{
  if(strcmp(password, attempt) == 0)
  {
    return 1;
  } else {
    return 0;
  }
}

int main()
{

	char password[128] = "VerySecretPassword";
	char attempt[128];

	while(1)
	{
		// Read the next line!
		printf("Please Try To Guess My Password\n> ");
		scanf("%s", attempt);
		printf("Guess: %s\n", attempt);

		if(password_cmp(password,attempt))
		{
			printf("Oh No! You Guessed My Password?! Take All My Treasure!\n");
			break;
		}
		else {
			printf("Nope! Wrong Password! Please Try Again!\n");
		}
	}
}
