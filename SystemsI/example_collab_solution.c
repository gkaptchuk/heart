#include "stdio.h"

int main()
{
	// How much they have spend
	int alice_spent_on_rent = 1950;
	int bob_spent_on_groceries = 317;
	int charlie_spent_on_utilities = 193;

	//Compute the total amount of money spent
	int total_expenditure = alice_spent_on_rent + bob_spent_on_groceries + charlie_spent_on_utilities;

	int one_share_rent = alice_spent_on_rent/3;
	int one_share_groceries = bob_spent_on_groceries/3;
	int one_share_utilities = charlie_spent_on_utilities/3;

	//Compute How Much Bob and Charlies Owe Alice
	int bob_to_alice = one_share_rent - one_share_groceries;
	int charlie_to_alice = one_share_rent - one_share_utilities;

	//Compute How Much Alice and Charlie Owe Bob
	int alice_to_bob = one_share_groceries - one_share_rent;
	int charlie_to_bob = one_share_groceries - one_share_utilities;

	//Compute How Much Alice and Bob Owe Charlie
	int alice_to_charlie = -1*charlie_to_alice;
	int bob_to_charlie = -1*charlie_to_bob;

	//Make sure nothing is less than zero, because that wont help anyone.
	if(bob_to_alice < 0) bob_to_alice=0;
	if(charlie_to_alice < 0) charlie_to_alice=0;
	if(alice_to_bob < 0) alice_to_bob=0;
	if(charlie_to_bob < 0) charlie_to_bob=0;
	if(alice_to_charlie < 0) alice_to_charlie=0;
	if(bob_to_charlie < 0) bob_to_charlie=0;


	printf("Total Amount Spent By Housemembers: %d\n", total_expenditure);
	printf("Alice Owes Bob: %d\n", alice_to_bob);
	printf("Alice Owes Charlie: %d\n", alice_to_charlie);
	printf("Bob Owes Alice: %d\n", bob_to_alice);
	printf("Bob Owes Charlie: %d\n", bob_to_charlie);
	printf("Charlie Owes Alice: %d\n", charlie_to_alice);
	printf("Charlie Owes Bob: %d\n\n", charlie_to_bob);

	printf("Alice Spent: %d\n", alice_spent_on_rent + alice_to_bob + alice_to_charlie - bob_to_alice - charlie_to_alice );
	printf("Bob Balance: %d\n", bob_spent_on_groceries + bob_to_alice + bob_to_charlie - alice_to_bob - charlie_to_bob);
	printf("Charlie Balance: %d\n", charlie_spent_on_utilities + charlie_to_bob + charlie_to_alice - alice_to_charlie - bob_to_charlie);



}