// Adapted From https://www.thegeekstuff.com/2013/06/buffer-overflow/
 
#include <stdio.h>
#include <string.h>

int main(void)
{
    char input_buffer[21];
    int guessed_correctly = 0;

    while(1)
    {
        printf("Please Try To Guess My Password\n> ");
        gets(input_buffer);

        if(strcmp(input_buffer, "iheartmyheartstudents"))
        {
            printf("Nope! Wrong Password! Please Try Again!\n");
        }
        else 
        {
             printf("Oh No! You Guessed My Password?!\n");
             guessed_correctly = 1;
        }
      
        if(guessed_correctly)
        {
          printf("Take All My Treasure!\n");
          break;
        }
    }
}
